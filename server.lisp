(ql:quickload :argot-server/muses/trinity)
(argot-server:launch-server
 :passthrough (list (make-instance 'argot-server/muses/trinity:trinity)
                    "javascript-typescript-stdio"
                    "pyls"))
