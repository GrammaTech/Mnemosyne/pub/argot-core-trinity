\documentclass[sigplan,screen]{acmart}

\begin{document}

\title{Lenses for Interactive Multi-Language Synthesis}

\author{Sam Estep}
\affiliation{\institution{GrammaTech}}

\author{Eric Schulte}
\affiliation{\institution{GrammaTech}}

\author{Paul Rodriguez}
\affiliation{\institution{GrammaTech}}

\begin{abstract}
  To provide programmers access to the many synthesis techniques being currently
  developed, we present a tool written in Common Lisp that uses Language Server
  Protocol and bidirectional lenses to translate simple mathematical programs
  synthesized by Trinity into real-world languages such as JavaScript and Python
  from within the comfort of a well-known editor such as Emacs.
\end{abstract}

\keywords{lenses, synthesis, languages, tools, translation}

\maketitle

\section{Introduction}

The rate of innovation in program synthesis has been accelerating in recent
years. Specifically, recent research has produced many new techniques for
synthesizing programs in various DSLs. Because of this diversity of languages
for synthesis targets, a programmer using a given language $L$ is limited in the
set of state-of-the-art synthesis techniques they can apply to their workflow:
the programmer may only use a technique that has a prototype which targets $L$.

To bridge the gap between real-world programming languages and current synthesis
techniques, we introduce an early prototype of Mnemosyne, which provides access
to different synthesis tools through popular editors such as Emacs. As a proof
of concept, Mnemosyne currently lets users synthesize simple math expressions in
JavaScript and Python by providing an interface to Trinity
\cite{martins_trinity_2019}.

We first briefly describe a sample interaction between a user and Trinity
through Mnemosyne in Section~\ref{sec:demo}, then give an overview of some of
the key ideas from Mnemosyne's architecture in Section~\ref{sec:ideas}, continue
with some noteworthy technical points from Mnemosyne's implementation in
Section~\ref{sec:impl}, and conclude with a discussion of related work in
Section~\ref{sec:related}.

\section{Demonstration}\label{sec:demo}

The reader is invited to follow along with this demo using the Docker image and
instructions available at the repository holding this paper's source
code.\footnote{\url{https://gitlab.com/GrammaTech/Mnemosyne/pub/argot-core-trinity}}

The current proof-of-concept Trinity integration lets the programmer guide
synthesis by providing test cases, which are simply written as
specially-formatted code comments. As mentioned, these can be given in
JavaScript:

\begin{verbatim}
// 4, 3 -> 3
// 6, 3 -> 9
// 1, 2 -> -2
// 1, 1 -> 0
\end{verbatim}

Or in Python:

\begin{verbatim}
# 4, 3 -> 3
# 6, 3 -> 9
# 1, 2 -> -2
# 1, 1 -> 0
\end{verbatim}

After connecting their editor to the running Mnemosyne server, the programmer
selects these four lines, and uses an editor command to ask the tool to
synthesize a math expression. After a few seconds, the server returns some
synthesized code, and the editor inserts it into the file, either as JavaScript:

\begin{verbatim}
// 4, 3 -> 3
// 6, 3 -> 9
// 1, 2 -> -2
// 1, 1 -> 0
y * (x - y)
\end{verbatim}

Or as Python:

\begin{verbatim}
# 4, 3 -> 3
# 6, 3 -> 9
# 1, 2 -> -2
# 1, 1 -> 0
(y * (x - y))
\end{verbatim}

As shown above, the two produced pieces of code correspond to the same
mathematical expression, but they are slightly different strings. This is
because (as will be discussed in the next section) Mnemosyne uses off-the-shelf
tools to generate
JavaScript\footnote{\url{https://github.com/davidbonnet/astring}} and
Python\footnote{\url{https://github.com/simonpercivall/astunparse}} code, and it
just so happens that the latter tool inserts extra parentheses where the former
tool does not.

\section{Ideas}\label{sec:ideas}

This extended abstract is primarily concerned with two aspects of Mnemosyne:
\begin{itemize}
\item The overall architecture that allows it to connect popular languages and
  development tools to state-of-the-art synthesis tools
\item The particular component that uses lenses to translate between
  synthesis-friendly DSLs and user-friendly languages
\end{itemize}

We split the main ideas into a couple categories that roughly correspond to
those two topics. For a more concrete description of the architecture/design of
Mnemosyne, see Section~\ref{sec:impl}.

\subsection{JSON}

In order to pull together several different languages and formats, an
uninteresting but key initial idea to our approach is to represent everything as
JSON\footnote{\url{https://www.json.org/json-en.html}} whenever possible,
particularly ASTs of programs. This allows us to make use of a few existing
technologies:

\subsubsection{Language Server Protocol}

Abstractly, Microsoft's Language Server
Protocol\footnote{\url{https://microsoft.github.io/language-server-protocol/}}
splits language-IDE integration into a client and a server, where the client
only cares about the IDE (not the language), the server only cares about the
language (not the IDE), and the two communicate with each other over a common
protocol. This allowed us to implement Mnemosyne as an LSP server which
essentially acts as a ``multiplexer'' from an LSP client to several different
synthesis tools, which we call ``Muses.''

\subsubsection{Parsers and code generators}

As mentioned in Section~\ref{sec:demo}, we use existing tools to generate code
in real-world languages; this is made possible because those tools accept either
JSON or something that can easily be translated to and from JSON. Although not
shown in the demo, we also make use of existing parsers for
JavaScript\footnote{\url{https://github.com/acornjs/acorn}} and
Python\footnote{\url{https://docs.python.org/3/library/ast.html}} in order to
achieve full bidirectional translation between each of these ``base languages''
and the Trinity DSL we use for synthesis.

\subsection{Lenses}

While our demo only translates in one direction (from Trinity output to
JavaScript or Python), Mnemosyne is actually far more general than that. We
arbitrarily chose ASTs from the language Idris 2 as an intermediate form which
the Trinity ``Muse'' outputs, and from that point the translation between an
Idris 2 AST and a string of JavaScript or Python source code is entirely
bidirectional. We achieve this by implementing the translations as lenses
\cite{foster_combinators_2007,bohannon_boomerang_2008,hofmann_symmetric_2011}.
Our translation module is comprised of a lens library written in Common Lisp, on
top of which translation lenses for JavaScript and Python (both targeting Idris
2 ASTs) are written.

\subsubsection{Quotient lenses}

Lenses per se are a bit of a brittle abstraction, so we instead base our
translations off of the concept of quotient lenses \cite{foster_quotient_2008},
which ignore ``irrelevant'' differences between ``equivalent'' values, such as
source code strings that differ only in whitespace. We work in the
dynamically-typed Common Lisp and thus leave the definition of ``equivalent''
implicit.

\subsubsection{``Discerning'' lenses}

On top of the base abstraction of quotient lenses, we define a new type of lens
which we call discerning lenses. A discerning lens is essentially a quotient
lens in which one equivalence class of ``ill-formed values'' is specially set
apart, with the rule that every ill-formed value must map to an ill-formed value
in either direction.

\subsubsection{Pattern matching}

Using the concept of discerning lenses, we are able to implement a sort of
bidrectional pattern-matching which makes it very easy to define translations.

\subsubsection{Lisp macros}

We implement all our translations using Common Lisp. The language does not have
built-in facilities for bidirectional pattern matching, but we were able to
implement it using Lisp macros.

\section{Implementation}\label{sec:impl}

This section highlights a few interesting details from the implementation of
Mnemosyne.

\subsection{Trinity}

Because Trinity only does synthesis in one direction (test cases to code) rather
than using program sketches or anything like that, we translate from Trinity
output to Idris 2 ASTs using a simple unidirectional Python function:

\begin{verbatim}
def translate(prog):
    # ...
    elif isinstance(prog, ApplyNode):
        op = prog.name
        if op == 'const':
            child, = prog.args
            return translate(child)
        else:
            left, right = prog.args
            ops = {
                'plus': '+',
                'minus': '-',
                'mult': '*',
            }
            return [
                'POp',
                ['EmptyFC'],
                ['UN', ops[op]],
                translate(left),
                translate(right),
            ]
\end{verbatim}

\subsection{Lenses}

The valuable part of our implementation is just how easy it makes it to write
bidirectional translations between programming languages. Once we have a JSON
format for the ASTs of both sides of the translation, we essentially just needs
to write ``template'' values in thet wo languages, specifying what translations
to use for the holes in the templates:

\begin{verbatim}
(-> arrow-lambda () l:lens)
(defun arrow-lambda ()
  (l:match ((param (id-term))
            (body (l:lazy #'term)))
    (fset:map
     ($ _)
     ("type" "ArrowFunctionExpression")
     ("id" nil)
     ("expression" t)
     ("generator" :false)
     ("async" :false)
     ("params" (seq param))
     ("body" body))
    (seq "PLam"
         (l:default (seq "EmptyFC"))
         (seq "RigW")
         (seq "Explicit")
         param
         (seq "PInfer"
              (l:default (seq "EmptyFC")))
         body)))
\end{verbatim}

The above example uses our custom bidirectional pattern-matching macro to define
a translation between JavaScript arrow-functions and Idris 2 lambdas.

\section{Related work}\label{sec:related}

Our style of partial language translation using discerning lenses and
bidirectional pattern matching was largely inspired by the concept of
incremental parametric syntax \cite{koppel_one_2018}.

\begin{acks}
  The project or effort depicted was sponsored by the Air Force Research
  Laboratory (AFRL) and the Defense Advanced Research Projects Agency (DARPA)
  under contract no. FA8750-15-C-0113. Any opinions, findings, and conclusions
  or recommendations expressed in this material are those of the author(s) and
  do not necessarily reflect the views of AFRL or DARPA.
\end{acks}

\bibliographystyle{ACM-Reference-Format}
\bibliography{refs}

\end{document}
