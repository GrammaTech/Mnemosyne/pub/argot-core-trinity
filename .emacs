(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
(package-install 'use-package)
(require 'use-package-ensure)
(setq use-package-always-ensure t)
(defun project-root (project)
  (car (project-roots project)))
(use-package eglot
  :config
  (add-to-list 'eglot-server-programs '(js-mode . ("localhost" 10003)))
  (add-to-list 'eglot-server-programs '(python-mode . ("localhost" 10003))))
(ido-mode 1)
(ido-everywhere 1)
(use-package ido-completing-read+
  :config
  (ido-ubiquitous-mode 1))
(setq confirm-kill-processes nil)
