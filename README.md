# Argot Workshop Paper

A workshop paper on the 2020 summer work writing bidirectional lenses between
JavaScript and a Trinity DSL then using these lenses to translate code
synthesized by Trinity back to JS for insertion into a larger JS program.

This repo assumes that you have installed [Docker][] and [TeX Live][].

## Running the demo in Docker

```
$ docker run -it registry.gitlab.com/grammatech/mnemosyne/pub/argot-core-trinity:$(./branch.sh)
```
This will open a split-screen view with a server log on the left and a terminal
on the right. From within the right-hand terminal, open one of the demo files
`demo.js` or `demo.py` in Emacs:
```
# emacs demo.$(shuf -en1 js py)
```
Once the server on the left displays the line `port: 10003`, [press][keys] `M-x
eglot` in Emacs to connect to it. Then press `C-x h` to select the four comment
lines in the buffer, followed by `M-x eglot-code-actions` to ask the server for
a list of synthesis options. The first option listed should be `synthesize math
expression from 4 test cases`, so press `<RET>` to choose it. After a couple
seconds, the server should ask permission to edit the file using the results of
the synthesis it performed. Press `y` to allow it, and observe the expression
that appears below the four comment lines.

To exit the demo, first press `C-u C-x C-c` to exit Emacs, then `C-d` twice to
exit both parts of the split-screen and leave the Docker container.

## Building the paper

```
$ latexmk -pdf
```
This will create a `main.pdf` file.

## Building the Docker image

```
$ docker build -t registry.gitlab.com/grammatech/mnemosyne/pub/argot-core-trinity:$(./branch.sh) .
```

[docker]: https://docs.docker.com/get-docker/
[keys]: https://www.gnu.org/software/emacs/manual/html_node/efaq/Basic-keys.html
[tex live]: https://www.tug.org/texlive/quickinstall.html
