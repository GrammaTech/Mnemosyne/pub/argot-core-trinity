FROM ubuntu:20.04

# general
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    curl emacs git python3-pip r-base sbcl tmux
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get update && apt-get install -y nodejs
RUN npm install --global acorn astring javascript-typescript-langserver
RUN pip3 install python-language-server[all]

# Quicklisp
RUN curl -O https://beta.quicklisp.org/quicklisp.lisp
RUN sbcl --load quicklisp.lisp \
    --eval '(quicklisp-quickstart:install)' \
    --eval '(let ((ql-util::*do-not-prompt* t)) (ql:add-to-init-file))'

# ASDF
RUN mkdir /root/common-lisp
RUN curl https://gitlab.common-lisp.net/asdf/asdf/-/archive/3.3.4.3/asdf-3.3.4.3.tar.gz \
    | tar xzC /root/common-lisp

# Argot
WORKDIR /root/quicklisp/local-projects
RUN git clone https://github.com/GrammaTech/cl-utils.git gt \
    && git -C gt checkout f1742623f4599c6777d10f901c505aa63a62b70b
RUN git clone https://github.com/GrammaTech/functional-trees.git \
    && git -C functional-trees checkout 08741c764bcb092081cbdcf517634e6b05538b1f
RUN git clone https://github.com/GrammaTech/sel.git \
    && git -C sel checkout c4dcf4b0b1902fa4a0f13f166d3dad98a9d55004
RUN git clone https://gitlab.com/GrammaTech/Mnemosyne/argot.git \
    && git -C argot checkout 8d816cfd7b3bc1b75d1cec01b811196f48357c0f
RUN git -C argot submodule update --init --recursive
RUN pip3 install -r argot/requirements.txt
RUN git clone https://gitlab.com/GrammaTech/Mnemosyne/jsonrpc.git \
    && git -C jsonrpc checkout bc2da62f6430bcb2a883fa7a0b30322f20da98b6
RUN git clone https://gitlab.com/GrammaTech/Mnemosyne/lsp-server.git \
    && git -C lsp-server checkout 26c900cdd6e3741ce32f4cf4233985c1e99a4c5e
RUN git clone https://gitlab.com/GrammaTech/Mnemosyne/argot-server.git \
    && git -C argot-server checkout 0a795d9115e37a5c2078ee92c620e10e5efeeeaf
RUN sbcl --eval "(ql:quickload :argot-server/muses/trinity)"

# Trinity
WORKDIR /
RUN git clone https://gitlab.com/GrammaTech/Mnemosyne/muses/trinity.git \
    && git -C trinity checkout e045270005c168b925c20d4b940a9bf89ceda054
WORKDIR /trinity
RUN pip3 install -r requirements.txt
RUN ln -s "$PWD/bin/trinity-muse" /usr/local/bin/trinity-muse
RUN git submodule update --init --recursive
WORKDIR /trinity/Trinity
RUN mkdir -p venv/bin
RUN touch venv/bin/activate
RUN pip3 install wheel
RUN pip3 install -e ".[dev]"

# Emacs
WORKDIR /root
COPY .emacs .
RUN emacs --script .emacs

# demo
WORKDIR /demo
COPY server.lisp demo.js demo.py ./
CMD tmux new-session -s session 'sbcl --load server.lisp' \; split-window -h
